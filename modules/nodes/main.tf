variable "ansible_inventory_path" {
}

# https://nicholasbering.ca/tools/2018/01/08/introducing-terraform-provider-ansible/
module "ansible_provisioner" {
  source    = "github.com/cloudposse/tf_ansible"
  arguments = ["-i ${var.ansible_inventory_path}", ]
  playbook  = "${path.module}/playbooks/master-playbook.yaml"
  dry_run   = false
}
